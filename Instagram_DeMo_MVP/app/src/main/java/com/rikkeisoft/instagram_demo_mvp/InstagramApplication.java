package com.rikkeisoft.instagram_demo_mvp;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Administrator on 7/5/2017.
 */

public class InstagramApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
