package com.rikkeisoft.instagram_demo_mvp.ui;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rikkeisoft.instagram_demo_mvp.R;
import com.rikkeisoft.instagram_demo_mvp.client.APIInterface;
import com.rikkeisoft.instagram_demo_mvp.data.DataRepository;
import com.rikkeisoft.instagram_demo_mvp.data.local.LocalDataResouce;
import com.rikkeisoft.instagram_demo_mvp.data.remote.RemoteDataSource;
import com.rikkeisoft.instagram_demo_mvp.data.viewmodel.PhotoViewModel;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;
import com.rikkeisoft.instagram_demo_mvp.util.APIUtils;
import com.rikkeisoft.instagram_demo_mvp.util.CheckConnection;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PictureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PictureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PictureFragment extends Fragment implements PictureContract.View, LifecycleRegistryOwner,View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private PicturePresenter m_Presenter;
    private DataRepository m_DataRepository;
    private PhotoViewModel m_viewModel;
    private RemoteDataSource m_remote;
    private APIInterface apiInterface;
    private LifecycleRegistry mRegistry;
    private PictureRecycleAdapter m_adapter;
    private PhotoViewPagerAdapter m_p_adapter;
    private RecyclerView m_rc_view;
    private ViewPager m_viewPager;
    private LocalDataResouce m_local;
    List<Photo> m_photos;
    private OnFragmentInteractionListener mListener;

    public PictureFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PictureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PictureFragment newInstance(String param1, String param2) {
        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        apiInterface = APIUtils.getService();
        m_viewModel = new PhotoViewModel(getActivity().getApplication());
        m_remote = new RemoteDataSource(apiInterface);
        m_local = new LocalDataResouce(getActivity().getApplication());
        m_DataRepository = new DataRepository(m_viewModel, m_remote, new CheckConnection(),m_local);
        m_Presenter = new PicturePresenter(this, m_DataRepository);
        m_photos = new ArrayList<>();
        m_adapter = new PictureRecycleAdapter(getContext().getApplicationContext(), m_photos,this);
        m_p_adapter = new PhotoViewPagerAdapter(getContext().getApplicationContext(),m_photos);

        mRegistry = new LifecycleRegistry(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_picture, container, false);
        m_rc_view = (RecyclerView) v.findViewById(R.id.rc_listPhotos);
        m_rc_view.setHasFixedSize(true);
        m_rc_view.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
//        m_rc_view.scrollToPosition(4);
        m_viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        m_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                m_rc_view.scrollToPosition(m_viewPager.getCurrentItem());
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        m_rc_view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
        });
        getPhotos(5);
        return v;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        m_rc_view.setAdapter(m_adapter);
        m_viewModel = ViewModelProviders.of(this).get(PhotoViewModel.class);
    }

    private void getPhotos(int page) {
        m_Presenter.getPhotos(getContext().getApplicationContext(), page);
//        m_viewModel.getListPhotoModel(page, Integer.parseInt(APIUtils.QUERY_PARAM_VALUE_PER_PAGE))
//                .observe(this, new Observer<List<Photo>>() {
//                    @Override
//                    public void onChanged(@Nullable List<Photo> photos) {
//                        m_adapter.addAll(photos);
//                    }
//                });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setUpProgressBar() {

    }

    @Override
    public void showPhoto(List<Photo> photos) {

        m_adapter.addAll(photos);
        m_rc_view.setAdapter(m_adapter);
        m_p_adapter.addAll(photos);
        m_viewPager.setAdapter(m_p_adapter);
    }

    @Override
    public void showErrorToast() {
        Toast.makeText(getContext(), "SAI ROI EM OI", Toast.LENGTH_SHORT).show();
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }

    @Override
    public void onClick(View v) {
        Integer position = (Integer) v.getTag(R.string.pos);
        m_viewPager.setCurrentItem(position);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
