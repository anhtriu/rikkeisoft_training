package com.rikkeisoft.instagram_demo_mvp.ui;

import android.content.Context;
import android.util.Log;

import com.rikkeisoft.instagram_demo_mvp.data.DataRepository;
import com.rikkeisoft.instagram_demo_mvp.data.DataSource;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */

public class PicturePresenter implements PictureContract.Presenter {
    PictureContract.View m_view;
    DataRepository m_Repository;
    public PicturePresenter(PictureContract.View m_view,
            DataRepository m_Repository){
        this.m_Repository = m_Repository;
        this.m_view = m_view;
    }

    @Override
    public void getPhotos(Context context, int page) {
        m_Repository.getPhotos(context, page, new DataSource.onLoadPicturesListener() {
            @Override
            public void onSuccess(List<Photo> photos) {
                Log.e("TAGGGGGG","0"+photos.size());
                m_view.setUpProgressBar();
                m_view.showPhoto(photos);

            }

            @Override
            public void onFailure(Throwable throwable) {
                m_view.showErrorToast();
            }

            @Override
            public void onNetworkFailure() {

            }
        });
    }
}
