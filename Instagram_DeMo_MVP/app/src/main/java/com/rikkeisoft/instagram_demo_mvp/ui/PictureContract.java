package com.rikkeisoft.instagram_demo_mvp.ui;

import android.content.Context;

import com.rikkeisoft.instagram_demo_mvp.model.Photo;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */

public class PictureContract {
    public interface View{
        void setUpProgressBar();
        void showPhoto(List<Photo> photos);
        void showErrorToast();
    }
    public interface Presenter{
        void getPhotos(Context context, int page);
    }
}
