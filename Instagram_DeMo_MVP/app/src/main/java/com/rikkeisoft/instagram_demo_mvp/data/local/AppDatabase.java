package com.rikkeisoft.instagram_demo_mvp.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.rikkeisoft.instagram_demo_mvp.model.Photo;

/**
 * Created by Administrator on 7/3/2017.
 */
@Database(entities = {Photo.class},version = 2)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;
    public static AppDatabase getDatabase(Context context){
        if(INSTANCE==null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"photo_db").build();
        }
        return INSTANCE;
    }
    public abstract PictureDAO dataModel();
}
