package com.rikkeisoft.instagram_demo_mvp.data.local;

import android.app.Application;
import android.os.AsyncTask;

import com.rikkeisoft.instagram_demo_mvp.data.DataSource;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;
import com.rikkeisoft.instagram_demo_mvp.util.APIUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Administrator on 7/5/2017.
 */

public class LocalDataResouce extends DataSource {
    private AppDatabase appDatabase;
    List<Photo> listPhotoViewModel;
    public LocalDataResouce(Application application){
        appDatabase = AppDatabase.getDatabase(application);
    }
    @Override
    public void getPhotos(int page, onLoadPicturesListener listener) {
        super.getPhotos(page, listener);
        listener.onSuccess(getListPhotoModel(page, Integer.parseInt(APIUtils.QUERY_PARAM_VALUE_PER_PAGE)));
    }
    public List<Photo> getListPhotoModel(int page, int perpage){
//        listPhotoViewModel = appDatabase.dataModel().getAllNoteNomal((page-1)*perpage,perpage);
        ArrayList<Integer> params = new ArrayList<>();
        params.add(page);
        params.add(perpage);
        try {
            listPhotoViewModel = new PhotoAsycn(appDatabase).execute(params).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return listPhotoViewModel;
    }
    public class PhotoAsycn extends AsyncTask<ArrayList<Integer>, Void, List<Photo>>{
        private AppDatabase db;
        public PhotoAsycn(AppDatabase db){
            this.db = db;
        }


        @Override
        protected List<Photo> doInBackground(ArrayList<Integer>... params) {
            return db.dataModel().getAllNoteNomal((params[0].get(0)-1),params[0].get(1));
        }

        @Override
        protected void onPostExecute(List<Photo> photos) {
            super.onPostExecute(photos);

        }
    }
}
