package com.rikkeisoft.instagram_demo_mvp.client;

import com.rikkeisoft.instagram_demo_mvp.model.Response;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Administrator on 7/3/2017.
 */

public interface APIInterface {
    @GET("services/rest/")
    Call<Response> getPhotos(@QueryMap Map<String, String> queryMap);
    }

