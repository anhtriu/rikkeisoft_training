package com.rikkeisoft.instagram_demo_mvp.data;

import android.content.Context;

import com.rikkeisoft.instagram_demo_mvp.data.local.LocalDataResouce;
import com.rikkeisoft.instagram_demo_mvp.data.viewmodel.PhotoViewModel;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;
import com.rikkeisoft.instagram_demo_mvp.util.CheckConnection;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */

public class DataRepository{
    private PhotoViewModel m_localDataViewModel;
    private DataSource m_remoteDataSource;
    private CheckConnection m_checkConnection;
    private LocalDataResouce m_localDataSource;
    public DataRepository(PhotoViewModel m_localDataViewModel, DataSource m_remoteDataSource,
                          CheckConnection m_checkConnection, LocalDataResouce m_localDataSource){
        this.m_checkConnection  = m_checkConnection;
        this.m_remoteDataSource = m_remoteDataSource;
        this.m_localDataViewModel = m_localDataViewModel;
        this.m_localDataSource = m_localDataSource;
    }
    public void getPhotos(Context context, final int page, final DataSource.onLoadPicturesListener callback){
        if(m_checkConnection.haveNetworkConnection(context)){
            m_remoteDataSource.getPhotos(page, new DataSource.onLoadPicturesListener() {
                @Override
                public void onSuccess(List<Photo> photos) {
                    callback.onSuccess(photos);
                    m_localDataViewModel.addAllPhotoToLocal(photos);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    callback.onFailure(throwable);
                }

                @Override
                public void onNetworkFailure() {

                }
            });
        }
        else {
            m_checkConnection.showErr(context,"KHONG CO INTERNET");
            m_localDataSource.getPhotos(page, new DataSource.onLoadPicturesListener() {
                @Override
                public void onSuccess(List<Photo> photos) {
                    callback.onSuccess(photos);

                }

                @Override
                public void onFailure(Throwable throwable) {

                }

                @Override
                public void onNetworkFailure() {

                }
            });
        }
    }
}
