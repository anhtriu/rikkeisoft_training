package com.rikkeisoft.instagram_demo_mvp.data.local;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.rikkeisoft.instagram_demo_mvp.model.Photo;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */
@Dao
public interface PictureDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPhotos(List<Photo> photos);
    @Query("select * from photo limit :start , :limit ")
    LiveData<List<Photo>> getAllNote(int start,int limit);
    @Query("select * from photo limit :start , :limit ")
    List<Photo> getAllNoteNomal(int start,int limit);
}
