package com.rikkeisoft.instagram_demo_mvp.ui;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rikkeisoft.instagram_demo_mvp.R;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;
import com.rikkeisoft.instagram_demo_mvp.util.Property;

import java.util.List;

/**
 * Created by Administrator on 7/6/2017.
 */

public class PhotoViewPagerAdapter extends PagerAdapter {
    private Context m_context;
    private List<Photo> photos;


    public PhotoViewPagerAdapter(Context m_context, List<Photo> photos) {
        this.m_context = m_context;
        this.photos = photos;
    }

    @Override
    public int getCount() {
        return photos.size();
    }
    public void addAll(List<Photo> listNote){
        this.photos = listNote;
        notifyDataSetChanged();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // Inflate the layout for the page
        LayoutInflater mLayoutInflater = LayoutInflater.from(m_context);
        View itemView = mLayoutInflater.inflate(R.layout.item_view_pager, container, false);
        // Find and populate data into the page (i.e set the image)
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imv_viewpager);
        // ...
        // Add the page to the container
        Photo photo = photos.get(position);
        final String url = String.format(Property.IMAGE_URL,photo.getFarm(),photo.getServer(),photo.getId(),photo.getSecret());
        Glide.with(m_context).load(url).into(imageView);
        container.addView(itemView);
        // Return the page
        return itemView;
    }

    // Removes the page from the container for the given position.
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
