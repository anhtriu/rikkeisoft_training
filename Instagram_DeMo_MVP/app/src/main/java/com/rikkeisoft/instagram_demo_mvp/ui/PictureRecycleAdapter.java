package com.rikkeisoft.instagram_demo_mvp.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rikkeisoft.instagram_demo_mvp.R;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;
import com.rikkeisoft.instagram_demo_mvp.util.Property;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */

public class PictureRecycleAdapter extends RecyclerView.Adapter<PictureRecycleAdapter.PhotoViewHolder>{
    private List<Photo> listPhoto;
    private Context context;
    private View.OnClickListener onClickListener;

    public PictureRecycleAdapter(Context context, List<Photo> listPhoto, View.OnClickListener onClickListener){
        this.context = context;
        this.listPhoto = listPhoto;
        this.onClickListener = onClickListener;

    }
    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.iem_picture,null);
        return new PhotoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PhotoViewHolder holder, final int position) {
        Photo photo = listPhoto.get(position);
        final String url = String.format(Property.IMAGE_URL,photo.getFarm(),photo.getServer(),photo.getId(),photo.getSecret());
        Glide.with(context).load(url).into(holder.imv_photo);
        holder.itemView.setTag(photo);
        holder.itemView.setTag(R.string.pos,position);
        holder.itemView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return listPhoto.size();
    }

    public void addAll(List<Photo> listNote){
        this.listPhoto = listNote;
        notifyDataSetChanged();
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imv_photo;
        PhotoViewHolder(View itemView) {
            super(itemView);
            imv_photo = (ImageView) itemView.findViewById(R.id.imv_pic);
        }
        @Override
        public void onClick(View v) {
        }
    }
}
