package com.rikkeisoft.instagram_demo_mvp.data.remote;

import android.util.Log;

import com.rikkeisoft.instagram_demo_mvp.client.APIInterface;
import com.rikkeisoft.instagram_demo_mvp.data.DataSource;
import com.rikkeisoft.instagram_demo_mvp.model.Response;
import com.rikkeisoft.instagram_demo_mvp.util.APIUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Administrator on 7/3/2017.
 */

public class RemoteDataSource extends DataSource {
    private APIInterface m_ApiInterface;
    public RemoteDataSource(APIInterface m_ApiInterface){
        this.m_ApiInterface = m_ApiInterface;
    }

    @Override
    public void getPhotos(int page, final onLoadPicturesListener listener) {
        super.getPhotos(page, listener);
        HashMap<String,String> map_query = new HashMap<>();
        map_query.put(APIUtils.QUERY_PARAM_METHOD,APIUtils.METHOD);
        map_query.put(APIUtils.QUERY_PARAM_API_KEY,APIUtils.API_KEY);
        map_query.put(APIUtils.QUERY_PARAM_PAGE, String.valueOf(page));
        map_query.put(APIUtils.QUERY_PARAM_PER_PAGE,APIUtils.QUERY_PARAM_VALUE_PER_PAGE);
        map_query.put(APIUtils.QUERY_PARAM_FORMAT,APIUtils.QUERY_PARAM_VALUE_JSON);
        map_query.put(APIUtils.QUERY_PARAM_NO_JSON_CALLBACK,APIUtils.QUERY_PARAM_VALUE_NO_JSON_CALLBACK);
        retrofit2.Call<Response> call = m_ApiInterface.getPhotos(map_query);
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if(response.isSuccessful()){
                    Response res = response.body();
                    Log.e("CO DU LIEU KHONG DAY",res.toString());
                    listener.onSuccess(res.getPhotos().getPhoto());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.e("CO DU LIEU KHONG DAY","SAI ROI A");
                    listener.onFailure(t);
            }
        });
    }

}
