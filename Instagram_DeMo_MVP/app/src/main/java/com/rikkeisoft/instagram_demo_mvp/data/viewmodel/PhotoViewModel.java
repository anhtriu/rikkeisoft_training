package com.rikkeisoft.instagram_demo_mvp.data.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.rikkeisoft.instagram_demo_mvp.data.local.AppDatabase;
import com.rikkeisoft.instagram_demo_mvp.model.Photo;

import java.util.List;

/**
 * Created by Administrator on 7/4/2017.
 */

public class PhotoViewModel extends AndroidViewModel{
    private AppDatabase appDatabase;
    LiveData<List<Photo>> listPhotoViewModel;
    public PhotoViewModel(Application application) {
        super(application);
        appDatabase = AppDatabase.getDatabase(this.getApplication());

    }
//    public LiveData<List<Photo>> getListPhotoModel(int page, int perpage){
//        listPhotoViewModel = appDatabase.dataModel().getAllNote((page-1)*perpage,perpage);
////        Log.e("SIZE LOCAL",String.valueOf(listPhotoViewModel.getValue().size()));
//        return listPhotoViewModel;
//    }
    public void addAllPhotoToLocal(List<Photo> photos){
        new AddPhotoAsync(appDatabase).execute(photos);
    }
    public class AddPhotoAsync extends AsyncTask<List<Photo>,Void,Void>{
        private AppDatabase db;

        public AddPhotoAsync(AppDatabase db) {
            this.db = db;
        }
        @Override
        protected Void doInBackground(List<Photo>... params) {
            appDatabase.dataModel().insertPhotos(params[0]);
            return null;
        }
    }
}
