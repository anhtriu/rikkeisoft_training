package com.rikkeisoft.instagram_demo_mvp.data;

import com.rikkeisoft.instagram_demo_mvp.model.Photo;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */

public abstract class DataSource {

    public interface onLoadPicturesListener {
        void onSuccess(List<Photo> photos);

        void onFailure(Throwable throwable);

        void onNetworkFailure();
    }


    public void getPhotos(int page, onLoadPicturesListener listener) {
    }
}
