package com.rikkeisoft.instagram_demo_mvp.util;

import com.rikkeisoft.instagram_demo_mvp.client.APIClient;
import com.rikkeisoft.instagram_demo_mvp.client.APIInterface;

/**
 * Created by Administrator on 7/4/2017.
 */

public class APIUtils {
    public static final String BASE_URL = "https://api.flickr.com";
    public static final String API_KEY = "6f1369ea42aff8473732f99f45e782e9";
    public static final String METHOD ="flickr.interestingness.getList";
    public static final String QUERY_PARAM_PHOTO_ID = "photo_id";
    public static final String QUERY_PARAM_API_KEY = "api_key";
    public static final String QUERY_PARAM_METHOD = "method";
    public static final String QUERY_PARAM_NO_JSON_CALLBACK = "nojsoncallback";
    public static final String QUERY_PARAM_FORMAT = "format";
    public static final String QUERY_PARAM_VALUE_JSON = "json";
    public static final String QUERY_PARAM_VALUE_NO_JSON_CALLBACK = "1";
    public static final String QUERY_PARAM_PER_PAGE = "per_page";
    public static final String QUERY_PARAM_VALUE_PER_PAGE = "10";
    public static final String QUERY_PARAM_PAGE = "page";
    public static APIInterface getService() {
        return APIClient.getClient(BASE_URL).create(APIInterface.class);
    }

}
