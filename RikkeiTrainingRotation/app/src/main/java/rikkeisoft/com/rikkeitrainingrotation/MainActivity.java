package rikkeisoft.com.rikkeitrainingrotation;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btn_addView;
    private RelativeLayout mainLayout;
    private TextView textView;
    private static String KEY ="Title";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null){
            btn_addView = (Button) findViewById(R.id.btn_hello);
            mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
            btn_addView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textView = new TextView(getApplicationContext());
                    textView.setText("This is a test");
                    mainLayout.addView(textView);
                }
            });
        }
        else {
            btn_addView = (Button) findViewById(R.id.btn_hello);
            mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
            textView = new TextView(getApplicationContext());
            textView.setText("This is a test");
            textView.setTextColor(Color.YELLOW);
            btn_addView.setBackgroundColor(Color.CYAN);
            mainLayout.addView(textView);
        }
        }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY,textView.getText().toString());
    }
}
