package rikkeisoft.com.rikkeinetworking_httpclienttraining;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {
    public static String url = "http://headers.jsontest.com/";
    private Button btnCount;
    private CountTotalCharacters count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCount = (Button) findViewById(R.id.btntest);
        count = new CountTotalCharacters();
        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count.execute(url);
            }
        });

    }

    public class CountTotalCharacters extends AsyncTask<String, Void, String> {
        InputStream in = null;
        BufferedReader reader = null;

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();

                HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

                in = httpResponse.getEntity().getContent();
                if (in != null) {
                    reader = new BufferedReader(new InputStreamReader(in));
                    String data = readData(reader);
                    return data;
                } else
                    return null;

            } catch (Exception e) {

            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
               btnCount.setText(result);
            } else {
                Log.e("Hihi"," Méo có dl");
            }
        }
    }

    private String readData(BufferedReader reader) {

        StringBuilder sb = new StringBuilder();
        String s = null;
        try {
            while ((s = reader.readLine()) != null) {
                sb.append(s);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(sb.length());
    }
}