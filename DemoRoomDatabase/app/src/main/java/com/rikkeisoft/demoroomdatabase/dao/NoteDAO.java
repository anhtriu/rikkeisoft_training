package com.rikkeisoft.demoroomdatabase.dao;

/**
 * Created by Administrator on 6/22/2017.
 */

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.rikkeisoft.demoroomdatabase.converter.DateConverter;
import com.rikkeisoft.demoroomdatabase.entity.Note;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

// interface này sẽ là nơi viết các hàm xử lý query đến database
// chứa notation @Dao để nhận biết
@Dao
@TypeConverters(DateConverter.class)
public interface NoteDAO {
    @Query("select * from notes")
    LiveData<List<Note>> getAllNote();

    @Query("select * from notes where id = :id")
    Note getNote(int id);
    @Insert(onConflict = REPLACE)
    long insertNote(Note note);
    @Update
    void update(Note note);
    @Delete
    void deleteNote(Note note);
}

