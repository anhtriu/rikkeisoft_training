package com.rikkeisoft.demoroomdatabase.view;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.adapter.RecyclerPictureAdapter;
import com.rikkeisoft.demoroomdatabase.entity.Note;
import com.rikkeisoft.demoroomdatabase.helper.AppHelper;
import com.rikkeisoft.demoroomdatabase.helper.BackgroundHelper;
import com.rikkeisoft.demoroomdatabase.helper.CameraHelper;
import com.rikkeisoft.demoroomdatabase.helper.Util;
import com.rikkeisoft.demoroomdatabase.listener.CameraListener;
import com.rikkeisoft.demoroomdatabase.listener.ChangeBackgroundListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.rikkeisoft.demoroomdatabase.helper.Util.m_color;
import static com.rikkeisoft.demoroomdatabase.helper.Util.m_totalMil;

public class AddActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, ChangeBackgroundListener, TimePickerDialog.OnTimeSetListener, CameraListener {
    private String m_dateCreate, m_timCreate;
    private AppHelper m_app_helper;
    private int m_nodeId;
    private int list_notes_length = 0;
    private BackgroundHelper m_BackgroundHelper;
    private List<Uri> myListPath;
    private List<Uri> myListPathAdd;
    private RecyclerPictureAdapter m_pictureAdapter;
    private CameraHelper m_cameraHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        a_init();
        setUpCreateTime();
        btnv_option.setVisibility(View.GONE);
        ln_alarm.setVisibility(View.GONE);
    }

    private void setUpCreateTime() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM", Locale.getDefault());
        Date date = new Date();
        m_dateCreate = format.format(date);
        SimpleDateFormat format1 = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date date1 = new Date();
        m_timCreate = format1.format(date1);
        SimpleDateFormat fm = new SimpleDateFormat("dd/MM HH:mm", Locale.getDefault());
        Date date2 = this.m_calender.getTime();
        this.tv_d_dateTime.setText(fm.format(date2));
    }


    private void a_init() {
        m_nodeId = 0;
        myListPath = new ArrayList<>();
        myListPathAdd = new ArrayList<>();
        m_cameraHelper = new CameraHelper(this);
        m_BackgroundHelper = new BackgroundHelper(this);
        m_app_helper = new AppHelper(this);
        m_pictureAdapter = new RecyclerPictureAdapter(getApplicationContext(), myListPath);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mn_camera:
                m_cameraHelper.startcameraAction();
                break;
            case R.id.mn_setBG:
                askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Util.CAMERA_REQUEST_CODE);
                askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Util.GALLERY_REQUEST_CODE);
                m_BackgroundHelper.setBGColor();
                break;
            case R.id.mn_createNote:
                createNote();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void createNote() {
        if (hasAlarm) {
            m_nodeId = (int) viewModel1.addNote(new Note(0, et_d_title.getText().toString(),
                    et_d_content.getText().toString(), m_dateCreate,
                    m_timCreate, m_color, TextUtils.join(",", this.myListPathAdd)));
            Toast.makeText(this, "" + m_nodeId, Toast.LENGTH_SHORT).show();

            try {
                m_app_helper.setAlarm(m_totalMil, et_d_title.getText().toString(), ++list_notes_length);
                Log.e("TIMEALARM", "" + m_totalMil);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            m_nodeId = (int) viewModel1.addNote(new Note(0, et_d_title.getText().toString(),
                    et_d_content.getText().toString(), m_dateCreate,
                    m_timCreate, m_color, TextUtils.join(",", this.myListPathAdd)));
            Toast.makeText(this, "" + m_nodeId, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        m_app_helper.checkResult(requestCode, resultCode, data, myListPath, m_pictureAdapter);
        myListPathAdd = myListPath;
        rc_picturepath.setAdapter(m_pictureAdapter);
    }

    @Override
    public void startProcess() {

        CharSequence options[] = new CharSequence[]{"Take Photos", "Open Galery"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Select your option:");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        m_app_helper.takePicture(rc_picturepath);

                        break;
                    case 1:
                        m_app_helper.openGallery(rc_picturepath);

                        break;

                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

}
