package com.rikkeisoft.demoroomdatabase.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.helper.AppHelper;
import com.rikkeisoft.demoroomdatabase.helper.Util;
import com.rikkeisoft.demoroomdatabase.listener.CameraListener;
import com.rikkeisoft.demoroomdatabase.listener.ChangeBackgroundListener;
import com.rikkeisoft.demoroomdatabase.viewmodel.AddNoteViewModel;
import com.rikkeisoft.demoroomdatabase.viewmodel.NoteListViewModel;

import java.util.Calendar;

import static com.rikkeisoft.demoroomdatabase.R.drawable.note;
import static com.rikkeisoft.demoroomdatabase.helper.Util.CAMERA_REQUEST_CODE;
import static com.rikkeisoft.demoroomdatabase.helper.Util.GALLERY_REQUEST_CODE;
import static com.rikkeisoft.demoroomdatabase.helper.Util.m_color;
import static com.rikkeisoft.demoroomdatabase.helper.Util.m_dayMilisecond;
import static com.rikkeisoft.demoroomdatabase.helper.Util.m_totalMil;

/**
 * Created by Administrator on 7/4/2017.
 *
 */


public abstract class BaseActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, CameraListener, ChangeBackgroundListener {
    protected EditText et_d_title, et_d_content;
    protected TextView tv_alarm, tv_d_dateTime;
    private Toolbar tb_d_toolbar;
    protected boolean hasAlarm = false;
    private DatePickerDialog dpDialog;
    private TimePickerDialog tpDialog;
    private ArrayAdapter<String> m_adapterDay;
    private ArrayAdapter<String> m_adapterTime;
    protected RecyclerView rc_picturepath;
    private AppHelper m_appHelper;
    private Dialog m_dialog;
    protected NoteListViewModel viewModel;
    protected AddNoteViewModel viewModel1;
    protected LinearLayout ln_alarm;
    protected LinearLayout btnv_option;
    private Spinner sp_time, sp_day;
    protected Calendar m_calender;
    protected ImageView imv_closeSpinner;
    private String[] m_arrDay, m_arrTime;
    protected RelativeLayout rl_rootLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        init();
        initSpinnerDetail();
        setUpToolbar();
        event();
    }

    protected void event() {
        tv_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_alarm.setVisibility(View.GONE);
                ln_alarm.setVisibility(View.VISIBLE);
                hasAlarm = true;
            }
        });
        imv_closeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ln_alarm.setVisibility(View.GONE);
                tv_alarm.setVisibility(View.VISIBLE);
                hasAlarm = false;
            }
        });
    }

    private void initSpinnerDetail() {
        m_arrDay = getResources().getStringArray(R.array.day);
        m_arrTime = getResources().getStringArray(R.array.time);
        m_adapterDay = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, m_arrDay);
        m_adapterTime = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, m_arrTime);
        sp_day.setAdapter(m_adapterDay);
        sp_time.setAdapter(m_adapterTime);
        sp_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        m_calender.set(Calendar.HOUR_OF_DAY, 0);
                        m_calender.set(Calendar.MINUTE, 0);
                        m_calender.set(Calendar.SECOND, 0);
                        m_dayMilisecond = m_calender.getTimeInMillis();
                        break;
                    case 1:
                        m_appHelper.resetDayToMilisecon();
                        m_dayMilisecond = m_dayMilisecond + 86400000;
                        break;
                    case 2:
                        break;
                    case 3:
                        dpDialog.show();
                        break;
                    default:
                        m_appHelper.resetDayToMilisecon();
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        m_appHelper.initTime(m_calender, 11, 52);
                        m_appHelper.resettotalMilisecon();
                        m_totalMil = m_dayMilisecond + m_calender.getTimeInMillis();

                        break;
                    case 1:
                        m_appHelper.initTime(m_calender, 7, 0);
                        m_appHelper.resettotalMilisecon();
                        m_totalMil = m_dayMilisecond + m_calender.getTimeInMillis();
                        break;
                    case 2:
                        m_appHelper.initTime(m_calender, 15, 10);
                        m_appHelper.resettotalMilisecon();
                        m_totalMil = m_dayMilisecond + m_calender.getTimeInMillis();
                        break;
                    case 3:
                        m_appHelper.initTime(m_calender, 18, 30);
                        m_appHelper.resettotalMilisecon();
                        m_totalMil = m_dayMilisecond + m_calender.getTimeInMillis();
                        break;
                    case 4:
                        tpDialog.show();
                        break;
                    default:
                        m_appHelper.resettotalMilisecon();
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    protected void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(BaseActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, permission)) {

                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{permission}, requestCode);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {

                case CAMERA_REQUEST_CODE:

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, GALLERY_REQUEST_CODE);
                    break;
                case GALLERY_REQUEST_CODE:
                    Intent gallery = new Intent(Intent.ACTION_GET_CONTENT);
                    gallery.setType("image/*");
                    startActivityForResult(gallery, CAMERA_REQUEST_CODE);
                    break;
            }

            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    protected void init() {
        setUpColorCard();
        rl_rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        viewModel = ViewModelProviders.of(this).get(NoteListViewModel.class);
        viewModel1 = ViewModelProviders.of(this).get(AddNoteViewModel.class);
        m_calender = Calendar.getInstance();
        tv_d_dateTime = (TextView) findViewById(R.id.d_datetime);
        btnv_option = (LinearLayout) findViewById(R.id.btnv_option);
        m_appHelper = new AppHelper(this);
        tpDialog = new TimePickerDialog(this, BaseActivity.this,
                m_calender.get(Calendar.HOUR_OF_DAY),
                m_calender.get(Calendar.MINUTE), true);
        dpDialog = new DatePickerDialog(this, BaseActivity.this,
                m_calender.get(Calendar.YEAR),
                m_calender.get(Calendar.MONTH),
                m_calender.get(Calendar.DAY_OF_MONTH));
        ln_alarm = (LinearLayout) findViewById(R.id.detailAlarm);
        tb_d_toolbar = (Toolbar) findViewById(R.id.d_toolbar);
        et_d_content = (EditText) findViewById(R.id.d_etContent);
        et_d_title = (EditText) findViewById(R.id.d_etTitle);
        tv_alarm = (TextView) findViewById(R.id.d_tv_alarm);
        sp_day = (Spinner) findViewById(R.id.spnDay);
        sp_time = (Spinner) findViewById(R.id.spnTime);
        imv_closeSpinner = (ImageView) findViewById(R.id.closeSpn);
        rc_picturepath = (RecyclerView) findViewById(R.id.listPicture);
        rc_picturepath.setHasFixedSize(true);
        rc_picturepath.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));

    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        m_appHelper.resetDayToMilisecon();
        m_dayMilisecond = m_appHelper.setDate(m_calender, year, month, dayOfMonth, m_arrDay);
        m_adapterDay.notifyDataSetChanged();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        m_appHelper.resettotalMilisecon();
        m_totalMil = m_dayMilisecond + m_appHelper.setTime(m_calender, hourOfDay, minute, m_arrTime);
        m_adapterTime.notifyDataSetChanged();
    }


    @Override
    public void changBG() {
        m_dialog = new Dialog(this);
        m_dialog.setContentView(R.layout.dialog_choose_bg);
        m_dialog.setTitle("Choose a background");
        ImageView imv_first = (ImageView) m_dialog.findViewById(R.id.imv_first);
        ImageView imv_second = (ImageView) m_dialog.findViewById(R.id.imv_second);
        ImageView imv_third = (ImageView) m_dialog.findViewById(R.id.imv_third);
        ImageView imv_fouth = (ImageView) m_dialog.findViewById(R.id.imv_fouth);
        m_dialog.show();
        imv_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.m_color = Color.CYAN;
                rl_rootLayout.setBackgroundColor(m_color);
                m_dialog.dismiss();

            }
        });
        imv_second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.m_color = Color.BLUE;
                rl_rootLayout.setBackgroundColor(m_color);
                m_dialog.dismiss();

            }
        });
        imv_third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.m_color = Color.YELLOW;
                rl_rootLayout.setBackgroundColor(m_color);
                m_dialog.dismiss();

            }
        });
        imv_fouth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.m_color = Color.RED;
                rl_rootLayout.setBackgroundColor(m_color);
                m_dialog.dismiss();

            }
        });
    }

    private void setUpColorCard() {
        Util.m_color = Color.WHITE;
    }

    private void setUpToolbar() {
        setSupportActionBar(tb_d_toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Note");
            getSupportActionBar().setIcon(note);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        tb_d_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
