package com.rikkeisoft.demoroomdatabase.helper;

import com.rikkeisoft.demoroomdatabase.listener.CameraListener;

/**
 * Created by Administrator on 29/06/2017.
 *
 */

public class CameraHelper {
    private CameraListener cameraListener;
    public CameraHelper(CameraListener cameraListener){
        this.cameraListener = cameraListener;
    }
    public void startcameraAction(){
        cameraListener.startProcess();
    }

}
