package com.rikkeisoft.demoroomdatabase.view;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.adapter.RecyclerPictureAdapter;
import com.rikkeisoft.demoroomdatabase.entity.Note;
import com.rikkeisoft.demoroomdatabase.helper.AppHelper;
import com.rikkeisoft.demoroomdatabase.helper.BackgroundHelper;
import com.rikkeisoft.demoroomdatabase.helper.CameraHelper;
import com.rikkeisoft.demoroomdatabase.helper.Util;
import com.rikkeisoft.demoroomdatabase.listener.CameraListener;
import com.rikkeisoft.demoroomdatabase.listener.ChangeBackgroundListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.rikkeisoft.demoroomdatabase.helper.Util.m_totalMil;

public class NoteDetailActivity extends BaseActivity implements ChangeBackgroundListener, CameraListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private int m_idNote;
    private int m_notePosfromMain;
    private ArrayList<Uri> m_listPath;
    private RecyclerPictureAdapter m_adapter;
    private ImageView imv_pre, imv_share, imv_delete, img_next;
    private boolean isResetAlarm;
    private CameraHelper m_cameraHelper;
    private BackgroundHelper m_bgHelper;
    private AppHelper m_appHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnv_option.setVisibility(View.VISIBLE);
        d_init();
        m_listPath = new ArrayList<>();
        getNoteInforFromMain();
        d_event();
    }

    private void d_event() {
        this.tv_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_alarm.setVisibility(View.GONE);
                ln_alarm.setVisibility(View.VISIBLE);
                isResetAlarm = true;
            }
        });
        this.imv_closeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ln_alarm.setVisibility(View.GONE);
                tv_alarm.setVisibility(View.VISIBLE);
                isResetAlarm = false;

            }
        });
        imv_pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPreNote();

            }
        });
        imv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareNote();
            }
        });
        imv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askToDeleteNote();
            }
        });
        img_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNextNote();
            }
        });
    }


    private void deleteNote() {
        List list = Util.myListNote;
        Note n = (Note) list.get(m_notePosfromMain);
        viewModel.deleteNote(n);
        finish();
    }

    private void askToDeleteNote() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Delete");
        b.setMessage("Are you sure you want to delete?");
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteNote();
                finish();
            }
        })
        ;
        b.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override

            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        b.create().show();
    }

    private void updateNote() {
        List list = Util.myListNote;
        Note n = (Note) list.get(m_notePosfromMain);
        n.setTitle(et_d_title.getText().toString());
        n.setContent(et_d_content.getText().toString());
        n.setPictureListPath(TextUtils.join(",", m_listPath));
        ColorDrawable viewColor = (ColorDrawable) rl_rootLayout.getBackground();
        int colorId = viewColor.getColor();
        n.setColor(colorId);
        viewModel1.updateNote(n);
        finish();
    }

    private void shareNote() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "" + et_d_content.getText().toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    private void checkButton() {
        List list = Util.myListNote;
        if (m_notePosfromMain == 0) {
            imv_pre.setEnabled(false);
            imv_pre.setAlpha(0.2f);
            img_next.setEnabled(true);
            img_next.setAlpha(1f);
        } else if (m_notePosfromMain == list.size() - 1) {
            img_next.setEnabled(false);
            img_next.setAlpha(0.2f);
            imv_pre.setEnabled(true);
            imv_pre.setAlpha(1f);
        } else if (m_notePosfromMain == 0 || m_notePosfromMain == list.size() - 1) {
            img_next.setEnabled(false);
            img_next.setAlpha(0.2f);
            imv_pre.setEnabled(false);
            imv_pre.setAlpha(0.2f);
        } else {
            img_next.setEnabled(true);
            imv_pre.setEnabled(true);
            imv_pre.setAlpha(1f);
            img_next.setAlpha(1f);
        }

    }

    private void showPreNote() {
        resetBGColorWhenNoteChange();
        m_listPath.clear();
        List list = Util.myListNote;
        this.m_notePosfromMain = this.m_notePosfromMain - 1;
        checkButton();
        if (m_notePosfromMain >= 0) {
            Note n = (Note) list.get(m_notePosfromMain);
            if (n.getPictureListPath().length() <= 1) {
                rc_picturepath.setVisibility(View.GONE);
            } else {
                String[] list1 = n.getPictureListPath().split(",");
                rc_picturepath.setVisibility(View.VISIBLE);

                for (String aList1 : list1) {
                    Uri uri = Uri.parse(aList1.trim());
                    m_listPath.add(uri);
                    m_adapter.notifyDataSetChanged();
                }
            }
            this.tv_d_dateTime.setText(n.getCreateDate() + " " + n.getCreateTime());
            this.et_d_title.setText(n.getTitle());
            this.et_d_content.setText(n.getContent());
            this.rl_rootLayout.setBackgroundColor(n.getColor());
        }
        Toast.makeText(this, "" + m_notePosfromMain, Toast.LENGTH_SHORT).show();
    }

    private void showNextNote() {
        resetBGColorWhenNoteChange();
        m_listPath.clear();
        List arrPhotos = Util.myListNote;
        this.m_notePosfromMain = this.m_notePosfromMain + 1;
        checkButton();
        if (m_notePosfromMain < arrPhotos.size()) {
            Note n = (Note) arrPhotos.get(m_notePosfromMain);
            if (n.getPictureListPath().length() <= 1) {
                rc_picturepath.setVisibility(View.GONE);
            } else {
                String[] list1 = n.getPictureListPath().split(",");
                rc_picturepath.setVisibility(View.VISIBLE);

                for (String aList1 : list1) {
                    Uri uri = Uri.parse(aList1.trim());
                    m_listPath.add(uri);
                    m_adapter.notifyDataSetChanged();
                }
            }
            tv_d_dateTime.setText(n.getCreateDate() + " " + n.getCreateTime());
            et_d_title.setText(n.getTitle());
            et_d_content.setText(n.getContent());
            this.rl_rootLayout.setBackgroundColor(n.getColor());
        }
        Toast.makeText(this, "" + m_notePosfromMain, Toast.LENGTH_SHORT).show();
    }

    private void resetBGColorWhenNoteChange() {
        this.rl_rootLayout.setBackgroundColor(Color.WHITE);
    }

    private void getNoteInforFromMain() {
        m_listPath.clear();
        Intent i = getIntent();
        Note note = (Note) i.getSerializableExtra("NOTE");
        if (note.getPictureListPath().length() <= 1) {
            rc_picturepath.setVisibility(View.GONE);
        } else {
            rc_picturepath.setVisibility(View.VISIBLE);
            String[] list = note.getPictureListPath().split(",");
            for (String aList : list) {
                Uri uri = Uri.parse(aList.trim());
                m_listPath.add(uri);
            }
        }
        m_adapter = new RecyclerPictureAdapter(getApplicationContext(), m_listPath);
        rc_picturepath.setAdapter(m_adapter);
        m_adapter.notifyDataSetChanged();
        m_notePosfromMain = i.getIntExtra("POS", 0);
        checkButton();
        Toast.makeText(this, "" + m_notePosfromMain, Toast.LENGTH_SHORT).show();
        m_idNote = note.getId();
        tv_d_dateTime.setText(note.getCreateDate() + " " + note.getCreateTime());
        et_d_title.setText(note.getTitle());
        et_d_content.setText(note.getContent());
        this.rl_rootLayout.setBackgroundColor(note.getColor());
    }

    private void d_init() {
        m_cameraHelper = new CameraHelper(this);
        m_bgHelper = new BackgroundHelper(this);
        m_appHelper = new AppHelper(this);
        img_next = (ImageView) findViewById(R.id.imv_next);
        imv_pre = (ImageView) findViewById(R.id.imv_pre);
        imv_share = (ImageView) findViewById(R.id.imv_share);
        imv_delete = (ImageView) findViewById(R.id.imv_delete);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnd_updatecamera:
                m_cameraHelper.startcameraAction();
                break;
            case R.id.mnd_updateBG:
                m_bgHelper.setBGColor();
                break;
            case R.id.mnd_updateNote:
                updateNote();
                if (isResetAlarm) {
                    try {
                        m_appHelper.setAlarm(m_totalMil, et_d_title.getText().toString(), ++m_idNote);
                        Log.e("Time", "" + m_totalMil);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    return true;
                }
                finish();
                break;
            case R.id.addOneNote:
                startActivity(new Intent(NoteDetailActivity.this, AddActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startProcess() {
        CharSequence options[] = new CharSequence[]{"Take Photos", "Open Galery"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Select your option:");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        m_appHelper.takePicture(rc_picturepath);
                        break;
                    case 1:
                        m_appHelper.openGallery(rc_picturepath);
                        break;
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        m_appHelper.checkResult(requestCode, resultCode, data, m_listPath, m_adapter);
    }

}
