package com.rikkeisoft.demoroomdatabase.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.rikkeisoft.demoroomdatabase.R;

import java.util.List;

/**
 * Created by Administrator on 28/06/2017.
 *
 */

public class RecyclerPictureAdapter extends RecyclerView.Adapter<RecyclerPictureAdapter.PictureViewHollder> {
    private Context mContext;
    private List<Uri> picturesList;

    public RecyclerPictureAdapter(Context mContext, List<Uri> picturesList) {
        this.mContext = mContext;
        this.picturesList = picturesList;
    }

    @Override
    public PictureViewHollder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PictureViewHollder(LayoutInflater.from(mContext).inflate(R.layout.item_picture,null));
    }

    @Override
    public void onBindViewHolder(PictureViewHollder holder, final int position) {
        Uri path = picturesList.get(position);
        holder.imv_pictureItem.setImageURI(path);
        holder.imb_deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picturesList.remove(position);
                notifyDataSetChanged();
            }
        });
//        Glide.with(mContext).load(path).into(holder.imv_pictureItem);
    }

    @Override
    public int getItemCount() {
        return picturesList.size();
    }
    class PictureViewHollder extends RecyclerView.ViewHolder{
        private ImageView imv_pictureItem;
        private ImageButton imb_deleteItem;
        PictureViewHollder(View itemView) {
            super(itemView);
            imv_pictureItem = (ImageView) itemView.findViewById(R.id.imv_picture);
            imv_pictureItem.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imv_pictureItem.setPadding(8, 8, 8, 8);
            imb_deleteItem = (ImageButton) itemView.findViewById(R.id.imb_removePicture);
        }
    }
}
