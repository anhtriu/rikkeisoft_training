package com.rikkeisoft.demoroomdatabase.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.rikkeisoft.demoroomdatabase.database.AppDatabase;
import com.rikkeisoft.demoroomdatabase.entity.Note;

import java.util.List;

/**
 * Created by Administrator on 6/22/2017.
 */

/**
 *   sử dụng viewmodel giúp cho dữ liệu đc an toàn ,
 *   n sẽ giữ nguyên trạng thái khi có sự thay đổi về config(tránh bị mất dl khi quay màn hình, etc...)
 */
public class NoteListViewModel extends AndroidViewModel{
    private AppDatabase appDatabase;
    private LiveData<List<Note>> listNoteViewModel;
    public NoteListViewModel(Application application) {
        super(application);
        appDatabase = AppDatabase.getDatabase(this.getApplication());
        listNoteViewModel = appDatabase.dataModel().getAllNote();
    }
    // sử dụng livedata chứa dữ liệu để cho Activity có thể quan sát đc dữ liệu bị thay đổi ntn và update lại trên UI
    public LiveData<List<Note>> getListNoteModel(){
        return listNoteViewModel;
    }

    public void deleteNote(final Note note) {
        new AddNoteAsync(appDatabase).execute(note);
    }

    private static class AddNoteAsync extends AsyncTask<Note, Void, Void> {
        private AppDatabase db;

        AddNoteAsync(AppDatabase db) {
            this.db = db;
        }

        @Override
        protected Void doInBackground(Note... params) {
            db.dataModel().deleteNote(params[0]);
            return null;
        }
    }
}
