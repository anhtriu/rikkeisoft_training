package com.rikkeisoft.demoroomdatabase.helper;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.rikkeisoft.demoroomdatabase.adapter.RecyclerPictureAdapter;
import com.rikkeisoft.demoroomdatabase.receiver.AlarmReceiver;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.rikkeisoft.demoroomdatabase.helper.Util.CAMERA_REQUEST_CODE;
import static com.rikkeisoft.demoroomdatabase.helper.Util.GALLERY_REQUEST_CODE;

/**
 * Created by Administrator on 6/29/2017.
 * support some method to use to setup camera and alarm
 */

public class AppHelper implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private Activity activity;

    public AppHelper(Activity c) {
        activity = c;
    }

    private static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void openGallery(RecyclerView rc) {
        Intent gallery = new Intent(Intent.ACTION_GET_CONTENT);
        gallery.setType("image/*");
        activity.startActivityForResult(gallery, CAMERA_REQUEST_CODE);
        rc.setVisibility(View.VISIBLE);
    }

    public void takePicture(RecyclerView rc) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        activity.startActivityForResult(intent, GALLERY_REQUEST_CODE);
        rc.setVisibility(View.VISIBLE);
    }

    public void setAlarm(long TimeInMillis, String t, int i) throws ParseException {
        Log.e("CURRENT", "" + System.currentTimeMillis());
        if (TimeInMillis > System.currentTimeMillis()) {
            Intent intent = new Intent(activity.getBaseContext(), AlarmReceiver.class);
            intent.putExtra("TEXT", t);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(activity.getBaseContext(), i, intent, 0);
            AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, TimeInMillis, pendingIntent);
        } else {
            Toast.makeText(activity, "Cannot set alarm", Toast.LENGTH_SHORT).show();
        }
    }

    public void checkResult(int requestCode, int resultCode, Intent data,
                            List<Uri> myListPath,
                            RecyclerPictureAdapter m_pictureAdapter) {
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            Uri path = AppHelper.getImageUri(activity.getApplicationContext(), bitmap);
            myListPath.add(path);
            m_pictureAdapter.notifyDataSetChanged();

        }
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri path = data.getData();
            Log.e("CONTACT", path.toString());
            myListPath.add(path);
            m_pictureAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }

    public void initTime(Calendar cal, int hour, int minute) {
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
    }

    public long setDate(Calendar m_calender, int year, int month, int dayOfMonth, String[] arrDay) {
        m_calender.set(Calendar.YEAR, year);
        m_calender.set(Calendar.MONTH, month);
        m_calender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        long m_dayMilisecond = m_calender.getTimeInMillis();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM", Locale.getDefault());
        Date date = m_calender.getTime();
        String m_date = format.format(date);
        arrDay[3] = m_date;
        return m_dayMilisecond;
    }

    public long setTime(Calendar m_calender, int hourOfDay, int minute, String[] arrTime) {
        m_calender.set(Calendar.HOUR_OF_DAY, hourOfDay);
        m_calender.set(Calendar.MINUTE, minute);
        long m_timeMilisecond = m_calender.getTimeInMillis();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date date = m_calender.getTime();
        String m_alarm = format.format(date);
        arrTime[4] = m_alarm;
        return m_timeMilisecond;
    }

    public void resetDayToMilisecon() {
        Util.m_dayMilisecond = 0;
    }

    public void resettotalMilisecon() {
        Util.m_dayMilisecond = 0;
    }
}


