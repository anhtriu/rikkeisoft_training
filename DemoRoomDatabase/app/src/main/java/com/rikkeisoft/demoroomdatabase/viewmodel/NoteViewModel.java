package com.rikkeisoft.demoroomdatabase.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;

import com.rikkeisoft.demoroomdatabase.database.AppDatabase;
import com.rikkeisoft.demoroomdatabase.entity.Note;

import java.util.concurrent.ExecutionException;

/**
 * Created by Administrator on 6/26/2017.
 */

public class NoteViewModel extends AndroidViewModel {
    private AppDatabase appDatabase;
    public NoteViewModel(Application application) {
        super(application);
        appDatabase = AppDatabase.getDatabase(this.getApplication());

    }
    public Note getSpecificNoteData(int noteId) {
        Note note = null;
        try {
            note = new GetNoteAsync(appDatabase).execute(noteId).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return note;
    }
    public static class GetNoteAsync extends AsyncTask<Integer, Void, Note> {
        private AppDatabase db;
        public GetNoteAsync(AppDatabase db) {
            this.db = db;
        }

        @Override
        protected Note doInBackground(Integer... params) {
            Note n = db.dataModel().getNote(params[0]);
           return n;
        }

        @Override
        protected void onPostExecute(Note note) {
            super.onPostExecute(note);
        }
    }
}
