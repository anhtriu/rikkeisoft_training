package com.rikkeisoft.demoroomdatabase.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;

import com.rikkeisoft.demoroomdatabase.database.AppDatabase;
import com.rikkeisoft.demoroomdatabase.entity.Note;

import java.util.concurrent.ExecutionException;

/**
 * Created by Administrator on 6/22/2017.
 */

public class AddNoteViewModel extends AndroidViewModel {
    private AppDatabase db;

    public AddNoteViewModel(Application application) {
        super(application);
        db = AppDatabase.getDatabase(application.getApplicationContext());
    }

    public long addNote(final Note note) {
        Long longId = null;
        try {
            longId = new AddNoteAsync(db).execute(note).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return longId;
    }
    public void updateNote(final Note note){
        new UpdateNoteAsync(db).execute(note);
    }

    private static class AddNoteAsync extends AsyncTask<Note, Void, Long> {
        private AppDatabase db;

        AddNoteAsync(AppDatabase db) {
            this.db = db;
        }

        @Override
        protected Long doInBackground(Note... params) {
            return   db.dataModel().insertNote(params[0]);
        }
    }
    private static class UpdateNoteAsync extends AsyncTask<Note, Void, Void> {
        private AppDatabase db;

        UpdateNoteAsync(AppDatabase db) {
            this.db = db;
        }

        @Override
        protected Void doInBackground(Note... params) {
            db.dataModel().update(params[0]);
            return null;
        }
    }
}
