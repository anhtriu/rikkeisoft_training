package rikkeisoft.com.rikkeinetworkingtraining;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static rikkeisoft.com.rikkeinetworkingtraining.R.id.tv_sodong;

public class MainActivity extends AppCompatActivity {
    TextView tvSodong;
    EditText et_url;
    Button btn_lineCounting;
    CheckConnection con;
    CountPageLineNumber count;
//    private static String url = "http://www.apps.coreservlets.com/NetworkingSupport/loan-calculator";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvSodong = (TextView) findViewById(tv_sodong);
        et_url = (EditText) findViewById(R.id.et_url);
        btn_lineCounting = (Button) findViewById(R.id.btn_count);
        count = new CountPageLineNumber(tvSodong);
        con = new CheckConnection();
        btn_lineCounting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = et_url.getText().toString();
                if (!url.equals("")) {
                    Toast.makeText(getApplicationContext(), "Bạn phải nhập vào đây", Toast.LENGTH_LONG).show();
                }
                else {
                    if (con.haveNetworkConnection(getApplicationContext())) {
                        count.execute("http://www.apps.coreservlets.com/NetworkingSupport/loan-calculator");
                    }
                    else {
                        con.showErr(getApplicationContext(), "Không có mạng");
                    }
                }
            }
        });

    }

//    public class CountPageLineNumber extends AsyncTask<String, Void, String> {
//        TextView tv_soDong;
//        public CountPageLineNumber(TextView tv_soDong) {
//            this.tv_soDong = tv_soDong;
//        }
//    // hàm trả về số dòng của page
//        @Override
//        protected String doInBackground(String... params) {
//            String textUrl = params[0];
//            InputStream in = null;
//            BufferedReader reader = null;
//            try {
//                // tạo kết nối và đọc dữ liệu
//                URL url = new URL(textUrl);
//                HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
//                httpConn.setAllowUserInteraction(false);
//                httpConn.setInstanceFollowRedirects(true);
//                httpConn.setRequestMethod("GET");
//                httpConn.connect();
//                int resCode = httpConn.getResponseCode();
//
//                if (resCode == HttpURLConnection.HTTP_OK) {
//                    in = httpConn.getInputStream();
//                    reader = new BufferedReader(new InputStreamReader(in));
//
//                    StringBuilder sb = new StringBuilder();
//                    String s = null;
//                    int i = 0;
//                    while ((s = reader.readLine()) != null) {
//                        sb.append(s);
//                        i++;
//                        sb.append("\n");
//                    }
//                    return String.valueOf(i);
//                } else {
//                    return null;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    in.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            if (result != null) {
//                this.tv_soDong.setText(result);
//            } else {
//
//            }
//        }
//    }
public class CountPageLineNumber extends AsyncTask<String, Void, String> {
    TextView tv_soDong;
    public CountPageLineNumber(TextView tv_soDong) {
        this.tv_soDong = tv_soDong;
    }
    // hàm trả về số dòng của page
    @Override
    protected String doInBackground(String... params) {
        String textUrl = params[0];
        InputStream in = null;
        BufferedReader reader = null;
        try {
            // tạo kết nối và đọc dữ liệu
            URL url = new URL(textUrl);
            String loanAmount = null;
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            int resCode = httpConn.getResponseCode();

            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
                reader = new BufferedReader(new InputStreamReader(in));

                StringBuilder sb = new StringBuilder();
                String s = null;
                while ((s = reader.readLine()) != null) {
                    sb.append(s);
                    JSONObject obj = new JSONObject(String.valueOf(sb));
                    loanAmount = obj.getString("loanAmount");

                }
                return loanAmount;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            this.tv_soDong.setText(result);
        } else {

        }
    }
}

}
