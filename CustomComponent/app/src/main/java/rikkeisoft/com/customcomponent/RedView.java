package rikkeisoft.com.customcomponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Administrator on 6/21/2017.
 */

public class RedView extends View implements View.OnClickListener {
    private Paint mBackgroundPaint;
    private int mDesiredSize = 400;
    private int mColor = Color.RED;
    private int mColorAfterPress = Color.YELLOW;

    public RedView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        // khai báo + gán để có thể sử dụng các thuộc tính trong xml
        TypedArray mType = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RedView,
                0, 0);
        try {
            mDesiredSize = mType.getDimensionPixelSize(R.styleable.RedView_mDesiredSize, mDesiredSize);
            mColor = mType.getColor(R.styleable.RedView_mColor, mColor);
            mColorAfterPress = mType.getColor(R.styleable.RedView_mColorAfterPress, mColorAfterPress);
        } finally {
            mType.recycle();
        }
        initPaint(mColor);

    }

    // khỏi tạo paint để vẽ lên canvas
    private void initPaint(int color) {
        mBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBackgroundPaint.setStyle(Paint.Style.FILL);
        mBackgroundPaint.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // vẽ hình vuông
        canvas.drawRect(0, 0, mDesiredSize, mDesiredSize, mBackgroundPaint);
    }

    // tính toán kích thước của view
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width, height;
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(mDesiredSize, widthSize);
        } else {
            width = mDesiredSize;
        }
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            height = Math.min(mDesiredSize, heightSize);
        } else {
            height = mDesiredSize;
        }
        setMeasuredDimension(width, height);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int eventAction = event.getAction();
        // vị trí của touch event
        int x = (int) event.getX();
        int y = (int) event.getY();

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                initPaint(mColorAfterPress);
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        // update lại canvas sau khi handle event
        invalidate();
        // đã xử lý touch event
        return true;
    }

    @Override
    public void onClick(View v) {

    }
}
