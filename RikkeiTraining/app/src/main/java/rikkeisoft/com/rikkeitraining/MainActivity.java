package rikkeisoft.com.rikkeitraining;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageButton ibtn_red, ibtn_blue;
    TextView tv_test;
    RadioGroup rg_group;
    RadioButton rd_red, rd_yellow, rd_blue;
    Button btn_setColor;
    ToggleButton tg_red,tg_yellow,tg_blue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        ibtn_blue.setOnClickListener(this);
        rd_blue.setOnClickListener(this);
        rd_yellow.setOnClickListener(this);
        rd_red.setOnClickListener(this);
        tg_yellow.setOnClickListener(this);
        tg_red.setOnClickListener(this);
        tg_blue.setOnClickListener(this);
        radioEvent();
        event();
    }

    private void radioEvent() {
        btn_setColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idChecked = rg_group.getCheckedRadioButtonId();
                if (idChecked == -1) {
                    return;
                }
                switch (idChecked) {
                    case R.id.rd_blue:

                        tv_test.setBackgroundColor(Color.BLUE);

                        break;
                    case R.id.rd_red:

                        tv_test.setBackgroundColor(Color.RED);

                        break;
                    case R.id.rd_yellow:

                        tv_test.setBackgroundColor(Color.YELLOW);

                        break;
                }
            }
        });
    }

    private void event() {
        ibtn_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_test.setBackgroundColor(Color.RED);
            }
        });




    }

    private void init() {
        ibtn_blue = (ImageButton) findViewById(R.id.ibtn_blue);
        ibtn_red = (ImageButton) findViewById(R.id.ibtn_red);
        tv_test = (TextView) findViewById(R.id.tv_test);
        rg_group = (RadioGroup) findViewById(R.id.rg_group);
        rd_blue = (RadioButton) findViewById(R.id.rd_blue);
        rd_red = (RadioButton) findViewById(R.id.rd_red);
        rd_yellow = (RadioButton) findViewById(R.id.rd_yellow);
        btn_setColor = (Button) findViewById(R.id.btnClick);
        tg_blue = (ToggleButton) findViewById(R.id.tg_blue);
        tg_red = (ToggleButton) findViewById(R.id.tg_red);
        tg_yellow = (ToggleButton) findViewById(R.id.tg_yellow);
    }

    @Override
    public void onClick(View v) {

        if (v == ibtn_blue) {
            tv_test.setBackgroundColor(Color.BLUE);
        }
        if (v == tg_blue) {
            tv_test.setBackgroundColor(Color.BLUE);
        }
        if (v == tg_red) {
            tv_test.setBackgroundColor(Color.RED);
        }
        if (v == tg_yellow) {
            tv_test.setBackgroundColor(Color.YELLOW);
        }
//        if(v==rd_blue){
//            tv_test.setBackgroundColor(Color.BLUE);
//        }
//        if(v==rd_red){
//            tv_test.setBackgroundColor(Color.RED);
//        }
//        if(v==rd_yellow){
//            tv_test.setBackgroundColor(Color.YELLOW);
//        }

    }
}
